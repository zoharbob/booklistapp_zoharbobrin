import * as types from '../constants/ActionTypes'

export const showModal = (isVisible) => ({
    type: types.SHOW_MODAL,
    payload: isVisible
})

export const showDeleteModal = (isVisible) => ({
    type: types.SHOW_DELETE_MODAL,
    payload: isVisible
}) 

export const fetchBooks = (bookList) => {
    return ({
        type: types.FETCH_BOOKS,
        payload: bookList
    })
}

export const onSelectBook = (book) => ({
    type: types.SELECT_BOOK,
    payload: book
})

export const onEditBook = (bookToEdit) => {
    return (dispatch, getState) => {

        var {title} = bookToEdit;
        var newTitle = modifyBookTitle(title);

        bookToEdit = {...bookToEdit, title: newTitle}   
        
        var book = getState().bookList.find(book => book.title === newTitle);
        if(book){
            return true;
        }

        dispatch({
            type: types.EDIT_BOOK,
            payload: bookToEdit
        });
    };
}

export const onAddBook = (bookToAdd) => {
    return (dispatch, getState) => {

        var {title} = bookToAdd;
        var newTitle = modifyBookTitle(title);
        
        bookToAdd = {...bookToAdd, title: newTitle}
        
        var book = getState().bookList.find(book => book.title === newTitle);
        if(book){
            return true;
        }

        dispatch({
            type: types.ADD_BOOK,
            payload: bookToAdd
        });
    };
}

export const onDeleteBook = (book) => ({
    type: types.DELETE_BOOK,
    payload: book
})

export const changeBookExistsVisibility = (isVisible) => ({
    type: types.BOOK_EXISTS_VISIBILITY,
    payload: isVisible
})

function modifyBookTitle(title){
    var newTitle = title.replace(/[^a-zA-Z ]/g, "");
    return newTitle.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}