import React from 'react';
import { Button, ButtonToolbar } from 'react-bootstrap';
import { connect } from 'react-redux';
import { showModal, onSelectBook, changeBookExistsVisibility} from '../actions/index';
import Dialog from './Dialog';

class BookDialog extends React.Component {

  handleShow() {
    this.props.showModal(true);
    if(this.props.book){
      this.props.onSelectBook(this.props.book);
      this.props.changeBookExistsVisibility(false);
    }
  } 

  render() {
    const { modalVisibility, showModal, bookExistVisibilityMessage } = this.props;    
     
    return (
        <ButtonToolbar>
          <Button bsStyle="primary" onClick={this.handleShow.bind(this)} style={{margin: '5px'}}>
            {this.props.children}
          </Button>

          <Dialog modalVisibility={modalVisibility} showModal={showModal} bookExistVisibilityMessage={bookExistVisibilityMessage} />
           
        </ButtonToolbar>  
    );
  }
}

const mapStateToProps = ({modalVisibility, bookExistVisibilityMessage}) => ({
  modalVisibility, 
  bookExistVisibilityMessage
})

export default connect(mapStateToProps, {showModal, onSelectBook, changeBookExistsVisibility})(BookDialog);