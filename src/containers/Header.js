import React from 'react';
import { Button, FormControl } from 'react-bootstrap';
import { Field, reduxForm } from "redux-form";
import { connect } from 'react-redux';
import { onAddBook } from '../actions/index'
import dateFormat from 'dateformat';

const required = value => value ? undefined : 'Required';

class Header extends React.Component {
    
    state = {
        bookExists: false
    }

    renderField({ input, label, meta: { touched, error } }){
        return (
            <div style={styles.FormControlStyle}>
                <FormControl
                    type={input.name !== 'date' ? 'text' : 'date'}
                    placeholder={label}
                    {...input}
                /> 
                {touched && (error && <span style={styles.errorMessage}>{error}</span>)} 
            </div>
        )
    }

    onSubmit(values){
        this.setState({
            bookExists: false
        })
        const { date } = values;
        var newDate = dateFormat(date, "mmmm d, yyyy");
        values = {...values, date: newDate};
        if(this.props.onAddBook(values)){
            this.setState({
                bookExists: true
            })
        }
    }

    render(){

        const { handleSubmit } = this.props;
        const { headerStyle, formContainer, FormControlStyle, AddBookContainer, errorMessage } = styles;

        return (
            <div style={headerStyle}>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <h1>Book List</h1>
                    <div style={formContainer}> 
                        <Field
                            label="Title"
                            name="title"
                            component={this.renderField}
                            validate={required}
                            style={FormControlStyle}
                        />
                        <Field
                            label="Author"
                            name="author"
                            component={this.renderField}
                            validate={required}                            
                            style={FormControlStyle}
                        />
                        <Field
                            label="Date"
                            name="date"
                            component={this.renderField}
                            validate={required}                            
                            style={FormControlStyle}
                        />
                        
                    </div>
                    <div style={AddBookContainer}>
                        <Button type='submit' bsStyle="primary" bsSize="large" block>
                            Add book
                        </Button>
                    </div>
                    {this.state.bookExists ? <h3 style={errorMessage}>Book title is already exists!</h3> : ''}                                 
                </form>
            </div>
        )
    }
    
}

const styles = {
    headerStyle: {
        backgroundColor: '#f8f9fa',
        padding: '10px',
        marginBottom: '10px',
        borderBottomWidth: 'thin',
        borderBottomStyle: 'solid'
    },
    FormControlStyle: {
        width: '30%'
    },
    formContainer: {
        maxWidth: '600px',
        margin: '0 auto 10px',
        display: 'flex',
        justifyContent: 'space-around' 
    },
    AddBookContainer: {
        maxWidth: '300px',
        margin: '0 auto 10px'
    },
    errorMessage: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: '15px'
    }
}

export default reduxForm({
  form: "addBook"
})(connect(null, { onAddBook })(Header));
 