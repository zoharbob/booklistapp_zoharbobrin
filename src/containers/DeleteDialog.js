import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import { showDeleteModal, onDeleteBook } from '../actions/index';

class DeleteDialog extends React.Component {
    constructor(props){
        super(props);

        this.renderDeleteDialog = false;
    }

    handleShow() {
        this.props.showDeleteModal(true);
        this.renderDeleteDialog = true;
    } 

    handleHide() {
        this.props.showDeleteModal(false);
    }

    deleteBook(){
        this.props.onDeleteBook(this.props.book);
        this.props.showDeleteModal(false);
    }

    renderDialog(){

        const { title } = this.props.book;

        if(this.renderDeleteDialog === true){
            return (
                <Modal show={this.props.deleteModalVisibility} onHide={this.handleHide.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            Delete Book
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>Do you really want to delete <b>{title}</b> ?</Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleHide.bind(this)}>Cancel</Button>
                        <Button bsStyle="danger" onClick={() => this.deleteBook()}>Yes</Button>
                    </Modal.Footer>
                </Modal> 
            )
        } 
    }
 

  render() {
    return (
        <div>
          <Button onClick={this.handleShow.bind(this)} style={{margin: '5px'}}>
            {this.props.children}
          </Button>  

          {this.renderDialog()}
        </div>  
    );
  }
}

const mapStateToProps = ({deleteModalVisibility}) => ({
    deleteModalVisibility
})

export default connect(mapStateToProps, {showDeleteModal, onDeleteBook})(DeleteDialog);


