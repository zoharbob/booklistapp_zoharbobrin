import React from 'react';
import { ListGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import ListItem from '../components/ListItem';
import { fetchBooks } from '../actions';
import axios from 'axios';

class bookList extends React.Component {

    componentDidMount(){
        axios.get('bookList.json').then(response => {
            this.props.fetchBooks(response.data);
        });
    }

    render(){
        const {bookList} = this.props;

         return (
             <div style={Styles.bookListStyle}>
                <ListGroup style={Styles.listGroupStyle}>
                    {bookList.map( book => <ListItem key={book.title} book={book}></ListItem>)}
                </ListGroup>
            </div>
         )
     }
}

const Styles = {
    bookListStyle: {
        display: 'flex',
        justifyContent: 'center'
    },
    listGroupStyle: {
        width: '800px'
    }
}

const mapStatetoProps = ({bookList}) => ({
    bookList
})

export default connect(mapStatetoProps, {fetchBooks})(bookList);