import React from 'react';
import { Button, Modal, FormControl, ControlLabel } from 'react-bootstrap';
import { Field, reduxForm } from "redux-form";
import { connect } from 'react-redux';
import { onEditBook, changeBookExistsVisibility } from '../actions/index'
import dateFormat from 'dateformat';

const required = value => value ? undefined : 'Required';

class Dialog extends React.Component {

    onSubmit(values){ 
        const { date } = values;
        var newDate = dateFormat(date, "mmmm d, yyyy");
        values = {...values, date: newDate};
        const currentTitle = this.props.initialValues.title;
        this.props.showModal(false);
        this.props.reset();
        if(this.props.onEditBook({currentTitle, ...values})){
            this.props.changeBookExistsVisibility(true);
            this.props.showModal(true);
        }
    }

    renderField({ input, meta: { touched, error } }){
      return (
            <div>
                <FormControl
                    type={input.name !== 'date' ? 'text' : 'date'}
                    placeholder="Enter text" 
                    onChange={input.onChange}
                    {...input}
                /> 
                 {(error && <span style={styles.errorMessage}>{error}</span>)} 
            </div>
      )
    }

    handleHide() { 
      this.props.showModal(false);
      
      if(! this.props.valid) {
        this.props.reset();        
      }
    }

    render(){

        const { handleSubmit, invalid } = this.props;
        
        return (
            <Modal 
                show={this.props.modalVisibility}
                onHide={this.handleHide.bind(this)}
                dialogClassName="custom-modal"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-lg">
                        Edit book details
                    </Modal.Title>
                </Modal.Header>

                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                  
                     <Modal.Body>
                  
                        <div>
                            <ControlLabel>Title</ControlLabel> 
                            <Field
                                label="Title For book"
                                name="title"
                                validate={required}
                                component={this.renderField} 
                            />
                        </div>
                        <div>
                            <ControlLabel>Author</ControlLabel> 
                            <Field
                                label="author For book"
                                name="author"
                                validate={required}
                                component={this.renderField}
                            />
                        </div>
                        <div>
                            <ControlLabel>Date</ControlLabel> 
                            <Field
                                label="Date For book"
                                name="date"
                                validate={required}
                                component={this.renderField}
                            />
                        </div> 
                     
                     </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleHide.bind(this)}>Cancel</Button>
                        <Button type='submit' bsStyle="primary" onClick={this.handleHide.bind(this)} disabled={invalid}>Save</Button>
                    </Modal.Footer>
                </form>

                {/* {this.state.bookExists ? <h3 style={styles.errorMessage}>Book title is already exists!</h3> : ''}     */}
                <div style={{textAlign: 'center'}}>
                    {this.props.bookExistVisibilityMessage ? <h3 style={styles.errorMessage}>Book title is already exists!</h3> : ''}                                 
                </div>
                
            </Modal>
        )
    }
}

 
const mapStateToProps = ({selectedBook}) => {

    var date = dateFormat(selectedBook.date, "isoDate")

    return {
        initialValues: { title: selectedBook.title, author: selectedBook.author, date }
    }
}

Dialog = reduxForm({
    form: "editForm",
    enableReinitialize: true
})(Dialog);

const styles = {
    errorMessage: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: '15px'
    }
}

export default connect(mapStateToProps, {onEditBook, changeBookExistsVisibility})(Dialog);

