import React, { Component } from 'react';
import './App.css';
import reducer from './reducers/index';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import BookList from './containers/BookList';
import Header from './containers/Header';
// import BookDialog from './containers/BookDialog'; 

const store = createStore(reducer, applyMiddleware(thunk));

class App extends Component {
  render() {
    return (
      <Provider store={store}>
          <div className="App"> 
            <Header></Header>
            <BookList />
          </div>
      </Provider>
    );
  }
}

export default App;
