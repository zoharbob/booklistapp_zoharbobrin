import { SELECT_BOOK } from "../constants/ActionTypes";

const selectBookReducer = (state = {}, action) => {
    switch(action.type){
        case SELECT_BOOK: {
            return action.payload;
        }

        default: return state;
    }
}

export default selectBookReducer;