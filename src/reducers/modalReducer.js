import { SHOW_MODAL } from "../constants/ActionTypes";

const modalReduccer = (state = false, action) => {

    switch(action.type){
        case SHOW_MODAL: {
            if(action.payload){
                return true;
            } 
            return false;
        }

        default: return state;
    }
}

export default modalReduccer;