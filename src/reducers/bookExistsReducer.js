import { BOOK_EXISTS_VISIBILITY } from "../constants/ActionTypes";

const bookExistsReducer = (state = false, action) => {
    switch (action.type){
        case BOOK_EXISTS_VISIBILITY: return action.payload;

        default: return state;
    }
}

export default bookExistsReducer;