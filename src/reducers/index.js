import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import modalReduccer from './modalReducer';
import listReducer from './listReducer';
import selectBookReducer from './selectBookReducer';
import deleteModalReduccer from './deleteModal';
import bookExistsReducer from './bookExistsReducer';

const reducer = combineReducers({
  form: reduxFormReducer, 
  modalVisibility: modalReduccer,
  deleteModalVisibility: deleteModalReduccer,
  bookList: listReducer,
  selectedBook: selectBookReducer,
  bookExistVisibilityMessage: bookExistsReducer
});

export default reducer;