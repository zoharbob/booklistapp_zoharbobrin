import { SHOW_DELETE_MODAL } from "../constants/ActionTypes";

const deleteModalReduccer = (state = false, action) => {

    switch(action.type){
        case SHOW_DELETE_MODAL: {
            if(action.payload){
                return true;
            } 
            return false;
        }

        default: return state;
    }
}

export default deleteModalReduccer;