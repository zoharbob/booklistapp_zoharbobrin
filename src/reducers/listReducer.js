import { FETCH_BOOKS, EDIT_BOOK, DELETE_BOOK, ADD_BOOK } from "../constants/ActionTypes";

const listReducer = (state = [], action) => {

    switch(action.type){
        case FETCH_BOOKS: {
            return action.payload;
        }
        case EDIT_BOOK: {
         
            const { currentTitle, ...book } = action.payload;
            var bookIndex = state.findIndex(book => book.title === action.payload.currentTitle);
            var bookList = state.slice();
            bookList.splice(bookIndex, 1, book);
            
            return bookList;
        }
        case DELETE_BOOK: {
            return state.filter( book => book.title !== action.payload.title);
        }
        case ADD_BOOK: {
            return [...state, action.payload];
        }

        default: return state;
    }
}

export default listReducer;