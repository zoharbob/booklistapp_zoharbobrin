export const SHOW_MODAL = 'SHOW_MODAL';
export const SHOW_DELETE_MODAL = 'SHOW_DELETE_MODAL';
export const FETCH_BOOKS = 'FETCH_BOOKS';
export const SELECT_BOOK = 'SELECT_BOOK';
export const EDIT_BOOK = 'EDIT_BOOK';
export const ADD_BOOK =  'ADD_BOOK';
export const DELETE_BOOK =  'DELETE_BOOK';
export const BOOK_EXISTS_VISIBILITY =  'DELETE_BOOK';