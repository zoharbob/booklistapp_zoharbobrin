import React from 'react';
import BookDialog from '../containers/BookDialog';
import { ListGroupItem } from 'react-bootstrap';
import DeleteDialog from '../containers/DeleteDialog';

const ListItem = ({book}) => {

    return (
        <ListGroupItem header="">
            <div>
                <h4><b>{book.title}</b></h4>
                <p>{book.author}</p>
                <p>{book.date}</p>
                <div style={styles.buttonsStyleContainer}>
                    <BookDialog book={book}>
                        Edit Book
                    </BookDialog>
                    <DeleteDialog book={book}>
                        Delete Book
                    </DeleteDialog> 
                </div>
            </div>
        </ListGroupItem> 
    )
}

const styles = {
    buttonsStyleContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    }
}

export default ListItem;